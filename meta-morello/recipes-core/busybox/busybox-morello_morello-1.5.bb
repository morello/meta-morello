require busybox-morello.inc

LIC_FILES_CHKSUM  = "file://LICENSE;md5=de10de48642ab74318e893a61105afbb \
                     file://archival/libarchive/bz/LICENSE;md5=28e3301eae987e8cfe19988e98383dae"

SRCBRANCH         = "morello/master"
SRCREV            = "424ea869b9445be4bbb0ccd9d4007c39d82e4992"
