require linux-morello.inc

LIC_FILES_CHKSUM  = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

SRCREV    = "fa6de65a2f2c6f3ef778e4dd46d1509591585221"

LINUX_VERSION           = "6.7"
LINUX_VERSION_EXTENSION = "-yocto-purecap"

COMPATIBLE_MACHINE = "morello"